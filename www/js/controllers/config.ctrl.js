angular.module('starter.configctrl', [])
.controller('ConfigCtrl', ['$scope', '$state', 'webService', function($scope, $state, webService){

	$scope.changePassw = function(){
		try{
			$scope.show();
			var passOld  = $scope.cliente.pass_old;
			var passNew  = $scope.cliente.pass_new;
			var passConf = $scope.cliente.pass_conf;
			webService.changePassw(passOld, passNew, passConf).then(function(data){
				if(data != null){
					$scope.hide();
					if(data.success){
						$scope.alert('Sucesso', data.message, 'home');
					}else{
						$scope.alert('Erro', data.message, null);
					}
				}
			});
		}catch(e){
			console.log ("Error Message: " + e.message);
		    $scope.hide();		
		}
	}

	$scope.updateSystem = function(){
		window.AppUpdate.checkAppUpdate(
			function(response){
				$scope.alert('Sucesso', 'Sistema já esta atualizado.');
			}, 
			function(error){
				$scope.alert('Erro', 'Ocorreu um erro ao atualizar sistema');
			}, 
			"http://www.tentrego.com.br/tentrego/android/version.xml"
		);
	}

	$scope.cliente = {};

	device = ionic.Platform.device();
	$scope.platform = device.platform;
	$scope.version = device.version;
	$scope.modelo = device.model;

	$scope.config = ionic.Platform;
	$scope.current = $state.current;

	// console.log($state.current.name);

	/*
	$scope.deviceInformation = ionic.Platform.device();
	$scope.isWebView = ionic.Platform.isWebView();
	$scope.isIPad = ionic.Platform.isIPad();
	$scope.isIOS = ionic.Platform.isIOS();
	$scope.isAndroid = ionic.Platform.isAndroid();
	$scope.isWindowsPhone = ionic.Platform.isWindowsPhone();
	$scope.currentPlatform = ionic.Platform.platform();
	$scope.currentPlatformVersion = ionic.Platform.version();

	console.log($scope.deviceInformation);
	console.log('Android: ' + $scope.isAndroid);
	console.log('Web: ' + $scope.isWebView);
	console.log('IPad: ' + $scope.isIPad);
	console.log('IOS: ' + $scope.isIOS);
	console.log('WindowsPhone: ' + $scope.isWindowsPhone);
	console.log('Plataforma: ' + $scope.currentPlatform);
	console.log('Plataforma Versão: ' + $scope.currentPlatformVersion);
	*/

}]);
