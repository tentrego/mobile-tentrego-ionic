angular.module('starter.ofertasctrl', [])
.controller('OfertasCtrl', ['$scope', '$state', '$stateParams','$ionicPopup', 'webService',
 function($scope, $state, $stateParams, $ionicPopup, webService){

 	$scope.message = '';
 	$scope.ofertas = {};
 	$scope.title = 'Carregando...';

	function getOfertas(categoria = null){
		$scope.ofertas = null;
		$scope.noofertas = false;
		try{
			$scope.show();
			webService.getOfertas(categoria).then(function(data){
				if(data != null){
					first = data[0];
					$scope.title = 'Ofertas' +  ((categoria) ? ' : ' + first.Servico.categoria : '');
					$scope.categoria = first;
					$scope.counter = data.length;
					$scope.ofertas = data;
					if(data.length > 0){
						$scope.message = 'Foram encontradas ' + data.length + ' ofertas.';	
					}else{
						$scope.message = 'Ofertas não encontradas.'	
					}
				}else{
					$scope.title   = 'Ofertas';
					$scope.message = 'Ofertas não encontradas.'
				}
				$scope.hide();
			});
		}catch(e){
			console.log ("Error Message: " + e.message);
		    $scope.hide();		
		}
	}

	function getOfertaById(id = null){
		$scope.ofertas = null;
		$scope.iconFavorito = ($scope.verFavorito(id)) ? 'ion-android-favorite' : 'ion-android-favorite-outline';
		try{
			$scope.show();
			webService.getOfertaById(id).then(function(data){
				$scope.oferta = data;
				console.log(data);
				$scope.title = 'Oferta: ' + data.Servico.nome;
				$scope.hide();
			});
		}catch(e){
			console.log ("Error Message: " + e.message);
		    $scope.hide();		
		}
	}

	function getOfertasByKeyword(keyword){
 		$scope.ofertas = null;
 		$scope.noofertas = true;
 		try{
 			$scope.show();
 			webService.getOfertasByKeyword(keyword).then(function(data){
 				if(data){
 					$scope.counter = data.length;
	 				$scope.ofertas = data;
	 				$scope.noofertas = false;
 				}
				$scope.hide(); 				
 			});
 		}catch(e){
			console.log ("Error Message: " + e.message);
   			$scope.hide();
 		}
	}

	if($stateParams.ofertaId){
		getOfertaById($stateParams.ofertaId);
	}else{
		getOfertas($stateParams.categoria);
	}


}]);