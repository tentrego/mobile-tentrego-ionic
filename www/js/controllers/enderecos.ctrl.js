angular.module('starter.enderecosctrl', [])
.controller('EnderecosCtrl', ['$scope', '$q', '$ionicModal', '$ionicPopup', '$stateParams', '$ionicListDelegate', 'myFunctions', 'webService', 
	function($scope, $q, $ionicModal, $ionicPopup, $stateParams, $ionicListDelegate, myFunctions, webService){

	$ionicModal.fromTemplateUrl('templates/addendereco.html', {
		scope: $scope,
		animation: 'slide-in-up'
	}).then(function(modal) {
		$scope.modal = modal;
	});

	$scope.openModalEnd = function() {
		$scope.modal.show();
	};

	$scope.closeModalEnd = function() {
		$scope.newend = {};
		$scope.select = {};
		$scope.modal.hide();
	};

	// Cleanup the modal when we're done with it!
	$scope.$on('$destroy', function() {
		$scope.modal.remove();
	});

	// Execute action on hide modal
	$scope.$on('modal.hidden', function() {
		// Execute action
	});

	// Execute action on remove modal
	$scope.$on('modal.removed', function() {
		// Execute action
	});

	function getEnderecoByCep(cep){
		try{
			webService.callWService('getEnderecoByCep', {cep:cep}).then(function(end){
				$scope.select.estado = {id:end.Endereco.estado_id, nome: end.Endereco.estado};
				myFunctions.getCidades(end.Endereco.estado_id).then(function(cidades){
					if(cidades.length > 0){
						$scope.cidades = cidades;
						$scope.select.cidade = {id:end.Endereco.cidade_id, nome: end.Endereco.cidade};
						myFunctions.getBairros(end.Endereco.cidade_id).then(function(bairros){
							if(bairros.length > 0){
								$scope.bairros = bairros;
								$scope.select.bairro = {id:end.Endereco.bairro_id, nome: end.Endereco.bairro};
								$scope.newend.estado_id = end.Endereco.estado_id;
								$scope.newend.cidade_id = end.Endereco.cidade_id;
								$scope.newend.bairro_id = end.Endereco.bairro_id;
								$scope.newend.endereco  = end.Endereco.logracompl;
								$scope.hide();
							}else{
								$scope.hide();
							}
						});
					}else{
						$scope.hide();
					}
				});
			});
		}catch(e){
			console.log ("Error Message: " + e.message);
		    $scope.hide();		
		}
	}

	$scope.loaderCity = function(){
		if($scope.select.estado != null){
			$scope.show('Carregando Cidades...');
			$scope.bairros = {};
			myFunctions.getCidades($scope.select.estado.id).then(function(cidades){
				if(cidades.length > 0) $scope.cidades = cidades;	
				$scope.hide();	
			});
		}else{
			$scope.cidades = {};
			$scope.hide();
		}
	}	

	$scope.loaderNeighborhood = function(){
		if($scope.select.cidade != null){
			$scope.show('Carregando Bairros...');
			myFunctions.getBairros($scope.select.cidade.id).then(function(bairros){
				if(bairros.length > 0) $scope.bairros = bairros;
				$scope.hide();		
			});	
		}else{
			$scope.bairros = {};
			$scope.hide();
		}
	}

	$scope.loaderAddressByCep = function(){
		if($scope.newend.cep != null && $scope.newend.cep.indexOf('_') < 0 && $scope.newend.cep != ''){
			$scope.show('Carregando Endereço...');
			getEnderecoByCep($scope.newend.cep);
		}
	}

	$scope.addNewEndereco = function(){
		$scope.show('Salvando Endereço...');
		if($scope.newend != null){
			$scope.getCoordenadas({
				street:$scope.newend.endereco,
				estado:$scope.select.estado.nome,
				cidade:$scope.select.cidade.nome,
				bairro:$scope.select.bairro.nome,
				numero:$scope.newend.numero
			}).then(function(coords){
				if(coords.success){
					$scope.newend.cliente_id = localStorage.getItem('cliente.id');
					$scope.newend.status = 1;
					$scope.newend.pais = 'BR';
					$scope.newend.latitude = coords.latitude;
					$scope.newend.longitude = coords.longitude;
					saveEndClient($scope.newend).then(function(resp){
						if(resp.success){
							myFunctions.getClienteEnderecos().then(function(enderecos){
								if(enderecos.length > 0){
									$scope.enderecos = enderecos;	
									myFunctions.getEstados().then(function(estados){
										if(estados.length > 0) $scope.estados = estados;
										$scope.closeModalEnd();
										$scope.hide();
										$scope.newend = {};
										$scope.alert('Salvo com Sucesso', 'Endereço Salvo com sucesso', null);
									});
								}	
							});
						}
					});		
				}
			});
		}
	}

	$scope.removeEnd = function(id, nome){
		var confirmPopup = $ionicPopup.confirm({
			title: 'Remover Endereço',
			template: 'Deseja remover ' + nome + ' ?',
			buttons: [{
				text: 'Excluir',
				type: 'button-dark',
				onTap: function(e) {
					$scope.show();
					delEnderecoEntregaById(id).then(function(resp){
						if(resp.success) getClienteEnderecos();
						$scope.hide();
					});
				}				
			},{
				text: 'Cancelar',
				type: 'button-dark',
				onTap: function(e) {}				
			}]
		});

	}

	$scope.editEnd = function(id, nome){
		$scope.show('Carregando ' + nome + '...');
		getEnderecoEntregaById(id).then(function(endereco){
			if(!endereco.success){
				$scope.newend = {
					id: endereco.Endereco.id,
					cep: endereco.Endereco.cep,
					tipo_endereco: endereco.Endereco.tipo_endereco,
					estado_id: endereco.Endereco.estado_id,
					cidade_id: endereco.Endereco.cidade_id,
					bairro_id: endereco.Endereco.bairro_id,
					endereco: endereco.Endereco.endereco,
					numero: endereco.Endereco.numero,
					complemento: endereco.Endereco.complemento,
					principal: (endereco.Endereco.principal == '1') ? true : false
				}
				$scope.select.estado = {id:endereco.Endereco.estado_id, nome: endereco.Endereco.estado};
				myFunctions.getCidades(endereco.Endereco.estado_id).then(function(cidades){
					if(cidades.length > 0){
						$scope.cidades = cidades;
						$scope.select.cidade = {id:endereco.Endereco.cidade_id, nome: endereco.Endereco.cidade };
						myFunctions.getBairros(endereco.Endereco.cidade_id).then(function(bairros){
							if(bairros.length > 0) $scope.bairros = bairros;
							$scope.select.bairro = {id:endereco.Endereco.bairro_id, nome: endereco.Endereco.bairro };
							$scope.hide();
						});
					}else{
						$scope.hide();
					}
				});
				$scope.openModalEnd();
			}
		});
	}

	function saveEndClient(dados){
		var defferer = $q.defer();
		try{
			webService.callWService('saveEndClient', { endereco : unescape(encodeURIComponent(JSON.stringify(dados))) }).then(function(enderecos){
				if(enderecos != null){
					$scope.enderecos = enderecos;
					defferer.resolve({success:true});
				}else defferer.resolve({success:false});
			});
		}catch(e){
			console.log ("Error Message: " + e.message);
		    defferer.resolve({success:false});
		}
		return defferer.promise;
	}

	function getEnderecoEntregaById(id){
		var defferer = $q.defer();
		try{
			webService.callWService('getEnderecoEntregaById', {id:id}).then(function(endereco){
				if(endereco != null){
					defferer.resolve(endereco);
				}else{ 
					defferer.resolve({success:false});
				}
			});
		}catch(e){
			console.log ("Error Message: " + e.message);
		    defferer.resolve({success:false});
		}
		return defferer.promise;
	}

	function delEnderecoEntregaById(id){
		var defferer = $q.defer();
		try{
			webService.callWService('delEnderecoEntregaById', {id:id}).then(function(endereco){
				if(endereco != null){
					defferer.resolve({success:true});
				}else{ 
					defferer.resolve({success:false});
				}
			});
		}catch(e){
			console.log ("Error Message: " + e.message);
		    defferer.resolve({success:false});
		}
		return defferer.promise;
	}

	function initialize(){
		var defferer = $q.defer();
		myFunctions.getClienteEnderecos().then(function(enderecos){
			if(enderecos.length > 0){
				$scope.enderecos = enderecos;
				myFunctions.getEstados().then(function(estados){
					if(estados.length > 0){
						$scope.estados = estados;
						defferer.resolve({success:true});
					}else{
						defferer.resolve({success:false});
					}
				});
			}else{
				defferer.resolve({success:false});
			}
		});
		return defferer.promise;
	}

	$scope.shouldShowDelete = false;
	$scope.shouldShowReorder = false;
	$scope.listCanSwipe = true;

	$scope.newend = {};
	$scope.select = {};

	$scope.show('Carregando Enderecos...');
	initialize().then(function(resp){
		$scope.hide();
	});

}]);
