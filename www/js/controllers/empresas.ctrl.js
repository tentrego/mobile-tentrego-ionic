angular.module('starter.empresasctrl', [])
.controller('EmpresasCtrl', ['$scope', '$state', '$stateParams','$ionicPopup', 'webService',
 function($scope, $state, $stateParams, $ionicPopup, webService){

 	$scope.message = '';
 	$scope.empresas = {};
 	$scope.title = 'Carregando...';

	function getEmpresas(categoria = null){
		$scope.show('Capturando Localização...');
		$scope.getGeoLocation().then(function(geo){
			if(geo.success){
				var lat = geo.latitude;
				var lng = geo.longitude;
			}else{
				$scope.alert('Erro', 'Não foram capturadas informações de Localidade (GPS)');
			}
			$scope.empresas = null;
			$scope.noempresas = false;
			$scope.show('Carregando Empresas...');
			try{
				webService.getEmpresas(categoria).then(function(data){
					if(data != null){
						first = data[0];
						$scope.title = 'Empresas' +  ((categoria) ? ' : ' + first.Empresa.categoria_mae : '');
						$scope.categoria = first;
						$scope.counter = data.length;
						$scope.empresas = data;
						if(data.length > 0){
							$scope.message = 'Foram encontradas ' + data.length + ' empresas.';	
						}else{
							$scope.message = 'Empresas não encontradas.'	
						}
					}else{
						$scope.title   = 'Empresas';
						$scope.message = 'Empresas não encontradas.'
					}
					$scope.hide();
				});
			}catch(e){
				console.log ("Error Message: " + e.message);
			    $scope.hide();		
			}
		});
		/*
		$scope.getGeoLocation();
		setTimeout(function() {
			var lat = localStorage.getItem('geo.latitude');
			var lng = localStorage.getItem('geo.longitude');
			if(lat != null && lng != null){
				$scope.empresas = null;
				$scope.noempresas = false;
				try{
					webService.getEmpresas(categoria).then(function(data){
						if(data != null){
							first = data[0];
							$scope.title = 'Empresas' +  ((categoria) ? ' : ' + first.Empresa.categoria_mae : '');
							$scope.categoria = first;
							$scope.counter = data.length;
							$scope.empresas = data;
							if(data.length > 0){
								$scope.message = 'Foram encontradas ' + data.length + ' empresas.';	
							}else{
								$scope.message = 'Empresas não encontradas.'	
							}
						}else{
							$scope.title   = 'Empresas';
							$scope.message = 'Empresas não encontradas.'
						}
						$scope.hide();
					});
				}catch(e){
					console.log ("Error Message: " + e.message);
				    $scope.hide();		
				}
			}else{
				getEmpresas(categoria);
			}
		}, 7000);		
		*/
	}

	function getEmpresaById(id = null){
		$scope.empresas = null;
		$scope.iconFavorito = ($scope.verFavorito(id)) ? 'ion-android-favorite' : 'ion-android-favorite-outline';
		try{
			$scope.show();
			webService.getEmpresaById(id).then(function(data){
				if(Object.keys(data).length > 0){
					$scope.empresa = data;	
					$scope.title = 'Empresa: ' + data.Empresa.nome_fantasia;
					$scope.hide();
				}else{
					$scope.hide();
					$scope.title   = 'Error';
					$scope.message = 'Empresas não encontradas.'
				}
			});
		}catch(e){
			console.log ("Error Message: " + e.message);
		    $scope.hide();		
		}
	}

	function getEmpresasByKeyword(keyword){
 		$scope.empresas = null;
 		$scope.noempresas = true;
 		try{
 			$scope.show();
 			webService.getEmpresasByKeyword(keyword).then(function(data){
 				if(data){
 					$scope.counter = data.length;
	 				$scope.empresas = data;
	 				$scope.noempresas = false;
 				}
				$scope.hide(); 				
 			});
 		}catch(e){
			console.log ("Error Message: " + e.message);
   			$scope.hide();
 		}
	}

	if($stateParams.EmpresaId){
		getEmpresaById($stateParams.EmpresaId);
	}else{
		getEmpresas($stateParams.categoria);
	}


}]);