angular.module('starter.carrinhoctrl', [])
.controller('CarrinhoCtrl', function($scope, $ionicPopup, $ionicListDelegate, webService){

	$scope.checkFavorito = function($id = null){
		$scope.show();
		var favoritos = {};
		id  = ($id) ? $id : $scope.oferta.Servico.id;
		if(typeof localStorage.favoritos != 'undefined') favoritos = JSON.parse(localStorage.getItem('favoritos'));
		if($scope.verFavorito(id)){
			// Removendo dos favoritos
			console.log('Removendo favorito');
			for(x = 0; x < favoritos.length; x++){
				if(favoritos[x].Servico.id == id) {
					var fav = favoritos[x];
					favoritos.splice(x, 1);
				}	
			}	
			$ionicPopup.alert({
				title: 'Remover Favorito',
				template: fav.Servico.nome + ' removido com sucesso dos favoritos.'
			});
			localStorage.setItem('favoritos', JSON.stringify(favoritos));
			$scope.iconFavorito = 'ion-android-favorite-outline';
		}else{
			// Adicionando aos favoritos
			console.log('Adicionando favorito');
			try{
				webService.getOfertaById(id).then(function(data){
					$ionicPopup.alert({
						title: 'Adicionar Favorito',
						template: data.Servico.nome + ' adicionado com sucesso aos favoritos.'
					});
					favoritos.push(data);
					localStorage.setItem('favoritos', JSON.stringify(favoritos));
				});
			}catch(e){
				console.log ("Error Message: " + e.message);
			}
			$scope.iconFavorito = 'ion-android-favorite';
		}
		$scope.hide();
	}

	$scope.addCompra = function($id = null){
		$scope.show();
		if($id){
			id = $id; 
			action = null;
		}else{
			id = $scope.oferta.Servico.id; 
			action = 'back';
		}
		var id = ($id) ? $id : $scope.oferta.Servico.id;
		try{	
			webService.getOfertaById(id).then(function(compra){
				var compras = new Array();
				var find = false;
				if(typeof sessionStorage.compras != undefined){
					compras = JSON.parse(sessionStorage.getItem('compras'));
				}
				if(compras != null){
					for(x = 0;x < compras.length; x++){
						if(id == compras[x].Servico.id){
							compras[x].Servico.quantidade = compras[x].Servico.quantidade + 1;
							find = true;
						}
					}
					if(!find){
						compra.Servico.quantidade = 1;
						compras.push(compra);
					}
				}else{
					compras = new Array();
					compra.Servico.quantidade = 1;
					compras.push(compra);
				}
				sessionStorage.setItem('compras', JSON.stringify(compras));
				$scope.hide();
				$scope.alert('Adicionado ao carrinho', compra.Servico.nome + ' foi adicionado ao carrinho de compras', action);
			});
		}catch(e){
			console.log ("Error Message: " + e.message);
		    $scope.hide();		
		}
	}	

	$scope.remCompra = function($nome, $id){
		var confirmPopup = $ionicPopup.confirm({
			title: 'Remover do Carrinho',
			template: 'Deseja remover ' + $nome + ' ?',
			buttons: [{
				text: 'Excluir',
				type: 'button-dark',
				onTap: function(e) {
					$scope.show();
					// e.preventDefault() will stop the popup from closing when tapped.
					var compras = JSON.parse(sessionStorage.getItem('compras'));
					for(x = 0;x < compras.length; x++) if(compras[x].Servico.id == $id) compras.splice(x, 1);
					sessionStorage.setItem('compras', JSON.stringify(compras));
					document.getElementById('cart_' + $id).remove();
					$scope.calcula();
					$scope.hide();
					// e.preventDefault();
				}				
			},{
				text: 'Cancelar',
				type: 'button-dark',
				onTap: function(e) {
					// e.preventDefault() will stop the popup from closing when tapped.
					// e.preventDefault();
				}				
			}]
		});
	}

	$scope.getcepvalue = function(cep_entrega = null){
		if(cep_entrega != null){
			if(cep_entrega.indexOf('_') < 0){
				$scope.show('Buscando Taxa de Entrega');
				try{
					webService.getTransportValue(cep_entrega).then(function(data){
						console.log(data);
						$scope.taxa_de_entrega = data.taxa_de_entrega;
						sessionStorage.setItem('cep_entrega', cep_entrega);
						sessionStorage.setItem('taxa_de_entrega', data.taxa_de_entrega.toFixed(2).replace('.', ','));
						$scope.calcula();
						$scope.hide();
					});
				}catch(e){
					console.log ("Error Message: " + e.message);
				    $scope.hide();		
				}
			}
		}
	}

	$scope.finalizaCompra = function(item){
		console.log(angular.element(item));
	}

	$scope.calcula = function(){
		console.log('Calculando....');
		$scope.subtotal = 0;
		$scope.taxa_de_entrega = parseFloat($scope.taxa_de_entrega);

		angular.forEach($scope.compras, function(obj){
			obj.Servico.total = (parseInt(obj.Servico.quantidade) * parseFloat(obj.Servico.valor)).toFixed(2).replace('.', ',');
			$scope.subtotal  += parseFloat(obj.Servico.total);
		});
		
		$scope.subtotal 	   = $scope.subtotal.toFixed(2).replace('.', ',');
		$scope.total 		   = (parseFloat($scope.subtotal) + parseFloat($scope.taxa_de_entrega)).toFixed(2).replace('.', ',');
		$scope.taxa_de_entrega = $scope.taxa_de_entrega.toFixed(2).replace('.', ',');	
		
		sessionStorage.setItem('sub_total', $scope.subtotal);
		sessionStorage.setItem('valor_total', $scope.total);
		sessionStorage.setItem('compras', JSON.stringify($scope.compras));
	}
	
	$scope.subtotal = 0.00;
	$scope.total = 0.00;
	$scope.taxa_de_entrega = 0.00;
	$scope.cep_entrega = sessionStorage.getItem('cep_entrega');

	if($scope.cep_entrega != '') $scope.getcepvalue($scope.cep_entrega);

	$scope.compras = JSON.parse(sessionStorage.getItem('compras'));

	$scope.nocompras = false;
	$scope.shouldShowDelete = false;
	$scope.shouldShowReorder = false;
	$scope.listCanSwipe = true;

	$scope.calcula();

});
