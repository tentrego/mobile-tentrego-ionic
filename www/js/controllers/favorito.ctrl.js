angular.module('starter.favoritoctrl', [])
.controller('FavoritoCtrl', ['$scope', '$ionicPopup', '$ionicListDelegate', 'webService', 
	function($scope, $ionicPopup, $ionicListDelegate, webService){

	$scope.favoritos = {};
	$scope.nofavoritos = true;

	$scope.checkFavorito = function($id = null){
		var favoritos = [];
		id  = ($id) ? $id : $scope.oferta.Servico.id;
		if(typeof localStorage.favoritos != 'undefined') favoritos = JSON.parse(localStorage.getItem('favoritos'));
		if($scope.verFavorito(id)){
			// Removendo dos favoritos
			console.log('Removendo favorito');
			for(x = 0; x < favoritos.length; x++){
				if(favoritos[x].Servico.id == id) {
					var fav = favoritos[x];
					favoritos.splice(x, 1);
				}	
			}	
			$ionicPopup.alert({
				title: 'Remover Favorito',
				template: fav.Servico.nome + ' removido com sucesso dos favoritos.'
			});
			localStorage.setItem('favoritos', JSON.stringify(favoritos));
			$scope.iconFavorito = 'ion-android-favorite-outline';
		}else{
			// Adicionando aos favoritos
			console.log('Adicionando favorito');
			try{
				webService.getOfertaById(id).then(function(data){
					$ionicPopup.alert({
						title: 'Adicionar Favorito',
						template: data.Servico.nome + ' adicionado com sucesso aos favoritos.'
					});
					favoritos.push(data);
					localStorage.setItem('favoritos', JSON.stringify(favoritos));
				});
			}catch(e){
				console.log ("Error Message: " + e.message);
			}
			$scope.iconFavorito = 'ion-android-favorite';
		}
	}

  	$scope.removeFavorito = function($nome, $id){
    	if(typeof localStorage.favoritos != 'undefined') favoritos = JSON.parse(localStorage.getItem('favoritos'));
		var confirmPopup = $ionicPopup.confirm({
			title: 'Remover Favorito',
			template: 'Deseja remover ' + $nome + ' ?',
			buttons: [{
				text: 'Excluir',
				onTap: function(e) {
					$scope.show();
					for(x = 0; x < favoritos.length; x++) if(favoritos[x].Servico.id == $id) favoritos.splice(x, 1);
					localStorage.setItem('favoritos', JSON.stringify(favoritos));
					document.getElementById('cart_' + $id).remove();
					$scope.hide();
					// e.preventDefault() will stop the popup from closing when tapped.
				}
			},{
				text: 'Cancelar',
				onTap: function(e) {
					// e.preventDefault() will stop the popup from closing when tapped.
				}
			}]
		});
	}

	$scope.addCompra = function($id = null){
		$scope.show();
		if($id){
			id = $id; 
			action = null;
		}else{
			id = $scope.oferta.Servico.id; 
			action = 'back';
		}
		var id = ($id) ? $id : $scope.oferta.Servico.id;
		try{	
			webService.getOfertaById(id).then(function(compra){
				var compras = new Array();
				var find = false;
				if(typeof sessionStorage.compras != undefined){
					compras = JSON.parse(sessionStorage.getItem('compras'));
				}
				if(compras != null){
					for(x = 0;x < compras.length; x++){
						if(id == compras[x].Servico.id){
							compras[x].Servico.quantidade = compras[x].Servico.quantidade + 1;
							find = true;
						}
					}
					if(!find){
						compra.Servico.quantidade = 1;
						compras.push(compra);
					}
				}else{
					compras = new Array();
					compra.Servico.quantidade = 1;
					compras.push(compra);
				}
				sessionStorage.setItem('compras', JSON.stringify(compras));
				$scope.hide();
				$scope.alert('Adicionado ao carrinho', compra.Servico.nome + ' foi adicionado ao carrinho de compras', action);
			});
		}catch(e){
			console.log ("Error Message: " + e.message);
		    $scope.hide();		
		}
	}		
	
	if(typeof localStorage.favoritos != 'undefined') $scope.favoritos = JSON.parse(localStorage.getItem('favoritos'));
	if($scope.favoritos.length > 0) $scope.nofavoritos = false;
	
	$scope.shouldShowDelete  = false;
	$scope.shouldShowReorder = false;
	$scope.listCanSwipe 	 = true;

}]);
