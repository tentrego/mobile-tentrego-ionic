angular.module('starter.dadosctrl', [])
.controller('DadosCtrl', ['$scope', '$timeout', 'Upload', 'webService', function($scope, $timeout, Upload, webService){

	$scope.cliente = $scope.getCliente();
	delete $scope.cliente.avatar;

	$scope.salvaCliente = function(){
		$scope.show('Salvando Cliente...');
		try{
			webService.saveCliente($scope.cliente).then(function(data){
				if(data != null){
					if(data.success){
						$scope.cliente.avatar = $scope.getAvatar();
						$scope.setCliente($scope.cliente);
						$scope.hide();	
						$scope.alert('Cliente', 'Dados Alterados com Sucesso.', 'home');
					}else{
						$scope.hide();
						$scope.alert('Erro', 'Ocorreu um erro ao Alterar os Dados.', 'home');
					}
				}else{
					$scope.hide();
					console.log('Erro ao salvar dados do cliente');
				}
			});		
		}catch(e){
			console.log ("Error Message: " + e.message);
			$scope.hide();
		}
	}

	$scope.upload = function (dataUrl, name) {
		$scope.show('Enviado...');
		Upload.upload({
			url: $scope.baseUrl + 'webservice/upload.php',
			method: 'POST',
			data: {
				id: localStorage.getItem('cliente.id'),
				file: Upload.dataUrltoBlob(dataUrl, name),
				name: name
			},
		}).then(function (response) {
			localStorage.setItem('cliente.avatar', response.data.avatar);
			$timeout(function () {
				$scope.result = response.data;
				$scope.avatar = response.data.avatar;
				$scope.alert('Avatar', 'Upload realizado com sucesso.', 'back');
				$scope.hide();
			});
		}, function (response) {
			if (response.status > 0){ 
				$scope.errorMsg = response.status  + ': ' + response.data;
				$scope.hide();
			}	
		}, function (evt) {
			$scope.progress = parseInt(100.0 * evt.loaded / evt.total);
		});
	}	

}]);
