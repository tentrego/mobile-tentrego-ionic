angular.module('starter.cadastroctrl', [])
.controller('CadastroCtrl', ['$scope', '$rootScope', '$q', '$stateParams', 'webService', 'myFunctions', function($scope, $rootScope, $q, $stateParams, webService, myFunctions){

	$scope.errors = null;
	$scope.new_avatar = null;
	$scope.cliente = {
		sexo:'M'
	};

	$scope.newend = {};

	function validaForm(){
		var error = [];
		if($scope.cliente.nome == '' || $scope.cliente.nome == undefined) error.push('Preencha o Nome');
		if($scope.cliente.sobrenome == '' || $scope.cliente.sobrenome == undefined) error.push('Preencha o Sobrenome');
		if($scope.cliente.email == '' || $scope.cliente.email == undefined) error.push('Preencha o Email');
		if($scope.cliente.passw == '' || $scope.cliente.passw == undefined) error.push('Preencha a Senha');
		if($scope.cliente.data_nascimento == '' || $scope.cliente.data_nascimento == undefined) error.push('Preencha a Data de Nascimento');
		if($scope.cliente.cpf == '' || $scope.cliente.cpf == undefined) error.push('Preencha o CPF');
		if($scope.cliente.telefone == ''  || $scope.cliente.telefone == undefined) error.push('Preencha o Telefone');
		if($scope.cliente.sexo == null) error.push('Selecione o Sexo');
		if($scope.cliente.passw != $scope.cliente.passw_conf) error.push('Senhas não conferem');
		if(error != ''){
			$scope.errors = error;
			return false;
		}else{
			return true;
		}
	}

	function saveEndereco(dados){
		var defferer = $q.defer();
		try{
			webService.callWService('saveEndClient', { endereco : unescape(encodeURIComponent(JSON.stringify(dados))) }).then(function(enderecos){
				if(enderecos != null){
					$scope.enderecos = enderecos;
					defferer.resolve({success:true});
				}else defferer.resolve({success:false});
			});
		}catch(e){
			console.log ("Error Message: " + e.message);
		    defferer.resolve({success:false});
		}
		return defferer.promise;
	}	

	function saveClient(dados){
		var defferer = $q.defer();
		try{
			webService.callWService('saveClient', { cliente : unescape(encodeURIComponent(JSON.stringify(dados))) }).then(function(cliente){
				if(cliente != null){
					defferer.resolve(cliente);
				}else{ 
					defferer.resolve({success:false}); 
				}
			});
		}catch(e){
			console.log ("Error Message: " + e.message);
		    defferer.resolve({success:false});
		}
		return defferer.promise;
	}

	$scope.addCliente = function(){
		$scope.errors = {};
		$scope.show('Salvando Cliente...');
		if(validaForm()){
			saveClient($scope.cliente).then(function(client){
				if(client.cliente_id){
					$scope.show('Buscando Coordenadas...');
					$scope.getCoordenadas({
						street: $scope.newend.endereco,
						estado: $scope.select.estado.nome,
						cidade: $scope.select.cidade.nome,
						bairro: $scope.select.bairro.nome,
						numero: $scope.newend.numero
					}).then(function(coords){
						if(coords.success){
							$scope.newend.latitude = coords.latitude;
							$scope.newend.longitude = coords.longitude;	
							$scope.newend.cliente_id = client.cliente_id;
							$scope.newend.status = 1;
							$scope.newend.principal = 1;
							$scope.newend.pais = 'BR';
							$scope.show('Salvando Endereço...');
							saveEndereco($scope.newend).then(function(resp){
								if(resp.success){
									localStorage.setItem('cliente', JSON.stringify($scope.cliente));
									localStorage.setItem('cliente.id', client.cliente_id);
									localStorage.setItem('cliente.nome', $scope.cliente.nome);
									localStorage.setItem('cliente.sobrenome', $scope.cliente.sobrenome);
									localStorage.setItem('cliente.email', $scope.cliente.email);
									// localStorage.setItem('cliente.avatar', cliente['avatar']);
									// localStorage.setItem('cliente.auth', cliente['auth']);
									$scope.alert('Cadastro de Cliente', 'Seu cadastro foi realizado com sucesso, em breve receberá um email para concluir a ativação.', 'home');
									$scope.login();
								}
								$scope.hide();
							});
						}
					});
				}else{
					$scope.hide();		
				}
			});
		}else{
			$scope.hide();
		}
	}

	function getEnderecoByCep(cep){
		try{
			webService.callWService('getEnderecoByCep', {cep:cep}).then(function(end){
				$scope.select.estado = {id:end.Endereco.estado_id, nome: end.Endereco.estado};
				myFunctions.getCidades(end.Endereco.estado_id).then(function(cidades){
					if(cidades.length > 0){
						$scope.cidades = cidades;
						$scope.select.cidade = {id:end.Endereco.cidade_id, nome: end.Endereco.cidade};
						myFunctions.getBairros(end.Endereco.cidade_id).then(function(bairros){
							if(bairros.length > 0){
								$scope.bairros = bairros;
								$scope.select.bairro = {id:end.Endereco.bairro_id, nome: end.Endereco.bairro};
								$scope.newend.estado_id = end.Endereco.estado_id;
								$scope.newend.cidade_id = end.Endereco.cidade_id;
								$scope.newend.bairro_id = end.Endereco.bairro_id;
								$scope.newend.endereco  = end.Endereco.logracompl;
								$scope.hide();
							}else{
								$scope.hide();
							}
						});
					}else{
						$scope.hide();
					}
				});
			});
		}catch(e){
			console.log ("Error Message: " + e.message);
		    $scope.hide();		
		}
	}

	$scope.loaderCity = function(){
		if($scope.select.estado != null){
			$scope.show('Carregando Cidades...');
			$scope.bairros = {};
			myFunctions.getCidades($scope.select.estado.id).then(function(cidades){
				if(cidades.length > 0) $scope.cidades = cidades;	
				$scope.hide();	
			});
		}else{
			$scope.cidades = {};
			$scope.hide();
		}
	}	

	$scope.loaderNeighborhood = function(){
		if($scope.select.cidade != null){
			$scope.show('Carregando Bairros...');
			myFunctions.getBairros($scope.select.cidade.id).then(function(bairros){
				if(bairros.length > 0) $scope.bairros = bairros;
				$scope.hide();		
			});	
		}else{
			$scope.bairros = {};
			$scope.hide();
		}
	}

	$scope.loaderAddressByCep = function(){
		if($scope.newend.cep != null && $scope.newend.cep.indexOf('_') < 0 && $scope.newend.cep != ''){
			$scope.show('Carregando Endereço...');
			getEnderecoByCep($scope.newend.cep);
		}
	}

	$scope.show('Carregando Estados...');
	myFunctions.getEstados().then(function(estados){
		$scope.estados = estados;
		$scope.hide();
	});


}]);
