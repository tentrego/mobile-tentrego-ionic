angular.module('starter.functions', [])
.service('myFunctions', ['$q', 'webService', function($q, webService){

	this.getEstados = function(){
		var defferer = $q.defer();
		try{
			webService.callWService('getEstados').then(function(estados){
				if(estados.length > 0){
					defferer.resolve(estados);	
				}else{
					defferer.resolve({});	
				}
			});
		}catch(e){
			console.log ("Error Message: " + e.message);
		    defferer.resolve({});
		}
		return defferer.promise;
	}

	this.getCidades = function(estado){
		var defferer = $q.defer();
		try{
			webService.callWService('getCidades', {id:estado}).then(function(cidades){
				if(cidades.length > 0){
					defferer.resolve(cidades);	
				}else{
					defferer.resolve({});	
				}
			});
		}catch(e){
			console.log ("Error Message: " + e.message);
		    defferer.resolve({});		
		}
	    return defferer.promise;
	}

	this.getBairros = function(cidade){
		var defferer = $q.defer();
		try{
			webService.callWService('getBairros', {id:cidade}).then(function(bairros){
				if(bairros.length > 0){
					defferer.resolve(bairros);	
				}else{
					defferer.resolve({});
				}
			});
		}catch(e){
			console.log ("Error Message: " + e.message);
		    defferer.resolve({});
		}
		return defferer.promise;
	}

	this.getClienteEnderecos = function(){
		var defferer = $q.defer();
		try{
			var cliente_id = localStorage.getItem('cliente.id');
			if(cliente_id){
				webService.callWService('getClienteEnderecos', {cliente_id:cliente_id}).then(function(enderecos){
					if(enderecos.length > 0){
						defferer.resolve(enderecos);	
					}else{
						defferer.resolve({});	
					}
				});
			}else{
				defferer.resolve({});
			}
		}catch(e){
			console.log ("Error Message: " + e.message);
			defferer.resolve({});
		}
		return defferer.promise;
	}


}]);