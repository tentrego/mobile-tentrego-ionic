angular.module('starter', [
	'ionic', 
	'ngCordova',
	'ui.mask', 
	'starter.controllers',
	'starter.services',
	'starter.functions'
]).run(function($ionicPlatform, $cordovaPush) {
	$ionicPlatform.ready(function() {
		if (window.cordova && window.cordova.plugins.Keyboard) {
			cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
			cordova.plugins.Keyboard.disableScroll(true);
		}
		if (window.StatusBar) {
			StatusBar.styleDefault();
		}
	});
}).config(function($stateProvider, $urlRouterProvider) {
	$stateProvider
	.state('app', {
		url: '/app',
		abstract: true,
		templateUrl: 'templates/menu.html',
		controller: 'AppCtrl'
	})
	.state('app.home', {
		url: '/home',
		views:{
			'menuContent':{
				templateUrl: 'templates/home.html',
				controller: 'HomeCtrl'
			}
		}
	})
	.state('app.empresas', {
		url: '/empresas/:categoria',
		views: {
			'menuContent':{
				templateUrl: 'templates/empresas.html',
				controller: 'EmpresasCtrl'
			}
		}
	})
	.state('app.empresa', {
		url: '/empresa/:EmpresaId',
		views: {
			'menuContent':{
				templateUrl: 'templates/empresa.html',
				controller: 'EmpresasCtrl'
			}
		}
	})
	.state('app.ofertas', {
		url: '/ofertas/:categoria',
		views: {
			'menuContent':{
				templateUrl: 'templates/ofertas.html',
				controller: 'OfertasCtrl'
			}
		}
	})
	.state('app.oferta', {
		url: '/oferta/:ofertaId',
		views: {
			'menuContent':{
				templateUrl: 'templates/oferta.html',
				controller: 'OfertasCtrl'
			}
		}
	})
	.state('app.dados', {
		url: '/dados',
		views: {
			'menuContent':{
				templateUrl: 'templates/dados.html',
				controller: 'DadosCtrl'
			}
		}
	})
	.state('app.avatar', {
		url: '/avatar',
		views: {
			'menuContent':{
				templateUrl: 'templates/avatar.html',
				controller: 'DadosCtrl'
			}
		}
	})
	.state('app.cadastro', {
		url: '/cadastro',
		views: {
			'menuContent':{
				templateUrl: 'templates/cadastro.html',
				controller: 'CadastroCtrl'
			}
		}
	})
	.state('app.favoritos', {
		url: '/favoritos',
		views: {
			'menuContent':{
				templateUrl: 'templates/favoritos.html',
				controller: 'FavoritoCtrl'
			}
		}
	})
	.state('app.carrinho', {
		url: '/carrinho',
		views: {
			'menuContent':{
				templateUrl: 'templates/carrinho.html',
				controller: 'CarrinhoCtrl'
			}
		}
	})
	.state('app.faq', {
		url: '/faq',
		views: {
			'menuContent':{
				templateUrl: 'templates/faq.html',
				controller: 'OfertasCtrl'
			}
		}
	})
	.state('app.enderecos', {
		url: '/enderecos',
		views: {
			'menuContent':{
				templateUrl: 'templates/enderecos.html',
				controller: 'EnderecosCtrl'
			}
		}
	})
	.state('app.pedidos', {
		url: '/pedidos',
		views: {
			'menuContent':{
				templateUrl: 'templates/pedidos.html',
				controller: 'PedidosCtrl'
			}
		}
	})
	.state('app.pedido', {
		url: '/pedido/:pedidoId',
		views: {
			'menuContent':{
				templateUrl: 'templates/pedido.html',
				controller: 'PedidosCtrl'
			}
		}
	})
	.state('app.config', {
		url: '/config',
		views: {
			'menuContent' : {
				templateUrl: 'templates/config.html',
				controller: 'ConfigCtrl'
			}
		}
	})
	.state('app.senha', {
		url: '/senha',
		views: {
			'menuContent' : {
				templateUrl: 'templates/senha.html',
				controller: 'ConfigCtrl'
			}
		}
	})
	.state('app.sobre', {
		url: '/sobre',
		views: {
			'menuContent' : {
				templateUrl: 'templates/sobre.html',
				controller: 'ConfigCtrl'
			}
		}
	})
	.state('app.search', {
		url: '/search',
		views: {
			'menuContent': {
				templateUrl: 'templates/search.html',
				controller: 'SearchCtrl'
			}
		}
	})
	.state('app.pagamento', {
		url: '/pagamento',
		views: {
			'menuContent':{
				templateUrl: 'templates/pagamento.html',
				controller: 'PagamentoCtrl'
			}
		}
	})
	.state('app.finalizado', {
		url: '/finalizado/:pedidoId',
		views: {
			'menuContent':{
				templateUrl: 'templates/finalizado.html',
				controller: 'PedidosCtrl'
			}
		}
	});
	$urlRouterProvider.otherwise('/app/home');
}).config(['uiMask.ConfigProvider', function(uiMaskConfigProvider){
	uiMaskConfigProvider.maskDefinitions({ '9': /\d/, 'A': /[a-zA-Z]/, '*': /[a-zA-Z0-9]/ });
	uiMaskConfigProvider.clearOnBlur(false); // true
	uiMaskConfigProvider.clearOnBlurPlaceholder(false); // false
	uiMaskConfigProvider.eventsToHandle(['input', 'keyup', 'click', 'focus', 'blur']);
	uiMaskConfigProvider.addDefaultPlaceholder(false); // true
	uiMaskConfigProvider.allowInvalidValue(false); // false
}]);

// Public Key: 5314ae2094ba079aebe5f53b2c978317bc578760eaee1daa
// Secret Key: 487583fa61fc778067796628a2cc45abc83b72626a9ab61c