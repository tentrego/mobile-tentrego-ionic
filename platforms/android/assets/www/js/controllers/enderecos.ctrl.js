angular.module('starter.enderecosctrl', [])
.controller('EnderecosCtrl', ['$scope', '$q', '$ionicModal', '$ionicPopup', '$stateParams', '$ionicListDelegate', 'webService', 
	function($scope, $q, $ionicModal, $ionicPopup, $stateParams, $ionicListDelegate, webService){

	$ionicModal.fromTemplateUrl('templates/addendereco.html', {
		scope: $scope,
		animation: 'slide-in-up'
	}).then(function(modal) {
		$scope.modal = modal;
	});

	$scope.openModalEnd = function() {
		$scope.modal.show();
	};

	$scope.closeModalEnd = function() {
		$scope.newend = {};
		$scope.select = {};
		$scope.modal.hide();
	};

	// Cleanup the modal when we're done with it!
	$scope.$on('$destroy', function() {
		$scope.modal.remove();
	});

	// Execute action on hide modal
	$scope.$on('modal.hidden', function() {
		// Execute action
	});

	// Execute action on remove modal
	$scope.$on('modal.removed', function() {
		// Execute action
	});

	$scope.loaderCity = function(){
		if($scope.select.estado != null){
			$scope.show('Carregando cidades...');
			$scope.bairros = {};
			getCidades($scope.select.estado.id).then(function(resp){
				$scope.hide();	
			});
		}else{
			$scope.cidades = {};
			$scope.hide();
		}
	}

	$scope.loaderNeighborhood = function(){
		if($scope.select.cidade != null){
			$scope.show('Carregando bairros...');
			getBairros($scope.select.cidade.id).then(function(resp){
				$scope.hide();		
			});	
		}else{
			$scope.bairros = {};
			$scope.hide();
		}
	}

	$scope.loaderAddressByCep = function(){
		if($scope.newend.cep != null && $scope.newend.cep.indexOf('_') < 0 && $scope.newend.cep != ''){
			$scope.show('Carregando Endereço...');
			getEnderecoByCep($scope.newend.cep);
		}
	}

	$scope.addNewEndereco = function(){
		if($scope.newend != null){
			$scope.getCoordenadas({
				street:$scope.newend.endereco,
				estado:$scope.select.estado.nome,
				cidade:$scope.select.cidade.nome,
				bairro:$scope.select.bairro.nome,
				numero:$scope.newend.numero
			}).then(function(coords){
				if(coords.success){
					$scope.newend.cliente_id = localStorage.getItem('cliente.id');
					$scope.newend.status = 1;
					$scope.newend.pais = 'BR';
					$scope.newend.latitude = coords.latitude;
					$scope.newend.longitude = coords.longitude;
					saveEndClient($scope.newend).then(function(resp){
						if(resp.success){
							getClienteEnderecos();
							$scope.newend = {};
							getEstados();
							$scope.closeModalEnd();
							alert('Endereço Salvo com sucesso');
						}
					});		
				}
			});
		}
	}

	$scope.removeEnd = function(id, nome){
		var confirmPopup = $ionicPopup.confirm({
			title: 'Remover Endereço',
			template: 'Deseja remover ' + nome + ' ?',
			buttons: [{
				text: 'Excluir',
				type: 'button-dark',
				onTap: function(e) {
					$scope.show();
					delEnderecoEntregaById(id).then(function(resp){
						if(resp.success) getClienteEnderecos();
						$scope.hide();
					});
				}				
			},{
				text: 'Cancelar',
				type: 'button-dark',
				onTap: function(e) {}				
			}]
		});

	}

	$scope.editEnd = function(id){
		getEnderecoEntregaById(id).then(function(endereco){
			if(!endereco.success){
				$scope.newend = {
					id: endereco.Endereco.id,
					cep: endereco.Endereco.cep,
					tipo_endereco: endereco.Endereco.tipo_endereco,
					estado_id: endereco.Endereco.estado_id,
					cidade_id: endereco.Endereco.cidade_id,
					bairro_id: endereco.Endereco.bairro_id,
					endereco: endereco.Endereco.endereco,
					numero: endereco.Endereco.numero,
					complemento: endereco.Endereco.complemento,
					principal: (endereco.Endereco.principal == '1') ? true : false
				}
				$scope.select.estado = {id:endereco.Endereco.estado_id, nome: endereco.Endereco.estado};
				getCidades(endereco.Endereco.estado_id).then(function(resp){
					if(resp.success){
						$scope.select.cidade = {id:endereco.Endereco.cidade_id, nome: endereco.Endereco.cidade };
						getBairros(endereco.Endereco.cidade_id).then(function(resp){
							$scope.select.bairro = {id:endereco.Endereco.bairro_id, nome: endereco.Endereco.bairro };
						});
					}
				});
				$scope.openModalEnd();
			}
			
		});
	}

	function saveEndClient(dados){
		var defferer = $q.defer();
		try{
			webService.callWService('saveEndClient', { endereco : unescape(encodeURIComponent(JSON.stringify(dados))) }).then(function(enderecos){
				if(enderecos != null){
					$scope.enderecos = enderecos;
					defferer.resolve({success:true});
				}else defferer.resolve({success:false});
			});
		}catch(e){
			console.log ("Error Message: " + e.message);
		    defferer.resolve({success:false});
		}
		return defferer.promise;
	}

	function getEnderecoByCep(cep){
		try{
			webService.callWService('getEnderecoByCep', {cep:cep}).then(function(end){
				$scope.select.estado = {id:end.Endereco.estado_id, nome: end.Endereco.estado};
				getCidades(end.Endereco.estado_id).then(function(resp){
					if(resp.success){
						$scope.select.cidade = {id:end.Endereco.cidade_id, nome: end.Endereco.cidade};
						getBairros(end.Endereco.cidade_id).then(function(resp){
							if(resp.success){
								$scope.select.bairro = {id:end.Endereco.bairro_id, nome: end.Endereco.bairro};
								$scope.newend.estado_id = end.Endereco.estado_id;
								$scope.newend.cidade_id = end.Endereco.cidade_id;
								$scope.newend.bairro_id = end.Endereco.bairro_id;
								$scope.newend.endereco  = end.Endereco.logracompl;
								$scope.hide();
							}else{
								$scope.hide();
							}
						});
					}else{
						$scope.hide();
					}
				});
			});
		}catch(e){
			console.log ("Error Message: " + e.message);
		    $scope.hide();		
		}
	}

	function getEnderecoEntregaById(id){
		var defferer = $q.defer();
		try{
			webService.callWService('getEnderecoEntregaById', {id:id}).then(function(endereco){
				if(endereco != null){
					defferer.resolve(endereco);
				}else{ 
					defferer.resolve({success:false});
				}
			});
		}catch(e){
			console.log ("Error Message: " + e.message);
		    defferer.resolve({success:false});
		}
		return defferer.promise;
	}

	function delEnderecoEntregaById(id){
		var defferer = $q.defer();
		try{
			webService.callWService('delEnderecoEntregaById', {id:id}).then(function(endereco){
				if(endereco != null){
					defferer.resolve({success:true});
				}else{ 
					defferer.resolve({success:false});
				}
			});
		}catch(e){
			console.log ("Error Message: " + e.message);
		    defferer.resolve({success:false});
		}
		return defferer.promise;
	}

	function getBairros(cidade){
		var defferer = $q.defer();
		$scope.bairros = {};
		try{
			webService.callWService('getBairros', {id:cidade}).then(function(bairros){
				if(bairros != null){
					$scope.bairros = bairros;
					defferer.resolve({success:true});
				}else defferer.resolve({success:false});
			});
		}catch(e){
			console.log ("Error Message: " + e.message);
		    defferer.resolve({success:false});
		}
		return defferer.promise;
	}

	function getCidades(estado){
		var defferer = $q.defer();
		$scope.cidades = {};
		try{
			webService.callWService('getCidades', {id:estado}).then(function(cidades){
				if(cidades != null){
					$scope.cidades = cidades;
					defferer.resolve({success:true});	
				} 
				else defferer.resolve({success:false});
			});
		}catch(e){
			console.log ("Error Message: " + e.message);
		    defferer.resolve({success:false});		
		}
	    return defferer.promise;
	}

	function getClienteEnderecos(){
		var defferer = $q.defer();
		try{
			$scope.enderecos = {};
			var cliente_id = localStorage.getItem('cliente.id');
			if(cliente_id){
				webService.callWService('getClienteEnderecos', {cliente_id:cliente_id}).then(function(enderecos){
					if(enderecos != null){
						defferer.resolve({success:true});	
						$scope.enderecos = enderecos;
					}	
				});
			}else{
				defferer.resolve({success:false});
				$scope.enderecos = null;
			}
		}catch(e){
			console.log ("Error Message: " + e.message);
			defferer.resolve({success:false});
		}
		return defferer.promise;
	}

	function getEstados(){
		var defferer = $q.defer();
		$scope.estados = {};
		$scope.cidades = {};
		$scope.bairros = {};
		try{
			webService.callWService('getEstados').then(function(estados){
				if(estados != null){
					$scope.estados = estados;
					defferer.resolve({success:true});	
				}else defferer.resolve({success:false});
			});
		}catch(e){
			console.log ("Error Message: " + e.message);
		    defferer.resolve({success:false});
		}
		return defferer.promise;
	}

	function initialize(){
		var defferer = $q.defer();
		getClienteEnderecos().then(function(resp){
			if(resp.success){
				getEstados().then(function(resp){
					if(resp.success){
						defferer.resolve({success:true});			
					}else{
						defferer.resolve({success:false});		
					}
				});
			}else{
				defferer.resolve({success:false});
			}
		});
		return defferer.promise;
	}

	$scope.shouldShowDelete = false;
	$scope.shouldShowReorder = false;
	$scope.listCanSwipe = true;

	$scope.newend = {};
	$scope.select = {};

	$scope.show('Carregando Enderecos...');

	initialize().then(function(resp){
		$scope.hide();
	});

}]);
