angular.module('starter.pedidosctrl', [])
.controller('PedidosCtrl', ['$scope', '$state', '$stateParams', 'webService', function($scope, $state, $stateParams, webService){

	$scope.efectPayment = function(id){
		$scope.show('Carregando Pedido...');
		sessionStorage.setItem('PedidoId', id);
		sessionStorage.setItem('sub_total', ($scope.pedido.Pedido.total_valor - $scope.pedido.Pedido.total_taxa_entrega));
		sessionStorage.setItem('valor_total', $scope.pedido.Pedido.total_valor);
		sessionStorage.setItem('cep_entrega', $scope.pedido.Pedido.cep_entrega);
		sessionStorage.setItem('taxa_de_entrega', $scope.pedido.Pedido.total_taxa_entrega);
		$state.go('app.pagamento');
	}

	function getPedidos(id){
		try{
			$scope.show('Carregando Pedidos...');
			webService.getPedidos(id).then(function(data){
				if(data != null) $scope.pedidos = data;
				else $scope.noresults = true;
				$scope.hide();
			});
		}catch(e){
			console.log ("Error Message: " + e.message);
		    $scope.hide();		
		}		
	}

	function getPedidoById(id){
		try{
			$scope.show('Carregando Pedido...');
			webService.getPedidoById(id).then(function(data){
				console.log(data);
				if(data != null){
					$scope.pedido 	= data;
					$scope.servicos = data.Servico;
					$scope.status 	= data.Pedido.status_label;
					$scope.message 	= data.Pedido.status_descricao;
					if($scope.status == 'Aprovada'){
						$scope.image = 'aprovado.jpg';
						$scope.classe = 'aprovada';
						$scope.newpgto = false;
					}else{
						$scope.image = 'reprovado.jpg';
						$scope.classe = 'reprovada';
						$scope.newpgto = true;
					}
				}else{
					$scope.noresults = true;
				}
				$scope.hide();
			});
		}catch(e){
			console.log ("Error Message: " + e.message);
		    $scope.hide();		
		}
	}

	$scope.pedidos = null;
	$scope.noresults = false;

	if($stateParams.pedidoId){
		getPedidoById($stateParams.pedidoId);
	}else{
		getPedidos(localStorage.getItem('cliente.id'));
	}

}]);
