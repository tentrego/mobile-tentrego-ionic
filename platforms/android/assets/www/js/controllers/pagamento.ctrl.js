angular.module('starter.pagamentoctrl', [])
.controller('PagamentoCtrl', ['$scope', '$ionicPopup', '$ionicSlideBoxDelegate', 'webService', '$q', function($scope, $ionicPopup, $ionicSlideBoxDelegate, webService, $q){

	var SHORT_NAME = 'mp-app-3598751';
	var PUBLIC_KEY = 'TEST-34834c85-d364-430b-b160-6e0d153bc9b1';
	var ACCESS_TOKEN = 'TEST-5999233168669110-070409-c45688c22a767978116eb22338694e3a__LD_LA__-3598751';
	
	$scope.getPaymentsInfo = {};
	$scope.errors = null;

	$scope.dadospagto  = {
		'token' : '',
		'email' : 'test_user_19653727@testuser.com',
		'cardNumber' : '4235 6477 2802 5682',
		'cardExpirationMonth' : 12,
		'cardExpirationYear' : 2020,
		'cardholderName' : 'APRO',
		'securityCode' : '123',
		'docType' : 'cpf',
		'docNumber' : '81782551034',
		'transactionAmount' : sessionStorage.getItem('valor_total'),
		'totalShipping' : sessionStorage.getItem('taxa_de_entrega'),
		'paymentMethodId' : '',
		'paymentTypeId' : 1,
		'pedidoId' : '',
		'nroEntrega' : ''
	};

	function getPaymentInfos(dados){
		$scope.flagCreditCard = dados.issuer.thumbnail;
		$scope.nameCreditCard = dados.issuer.name;
		$scope.getPaymentInfos = dados.payer_costs;
		$scope.dadospagto.installments = dados.payer_costs[0];
		$scope.dadospagto.paymentMethodId = dados.payment_method_id;
		$scope.hide();		
	}

	$scope.getPayments = function(){
		if($scope.dadospagto.cardNumber.length >= 6){
			var ccn = $scope.dadospagto.cardNumber.replace(' ', '').substr(0,6);
			Mercadopago.getInstallments({
				"bin": ccn,
				"amount": $scope.dadospagto.transactionAmount.replace(',', '.')
			}, function(status, response){
				if(status == 200) getPaymentInfos(response[0]);
			});			
		}
	}

	function getClienteEnderecos(){
		$scope.enderecos = {};
		try{
			webService.getClienteEnderecos().then(function(data){
				if(data != null){
					$scope.enderecos = data;
					angular.forEach(data, function(value, key){
  						if(value.Endereco.principal == 1) $scope.dadospagto.entrega = value;
					});
				}
			});
		}catch(e){
			console.log ("Error Message: " + e.message);
		    $scope.hide();		
		}
	}

	function getMercadoPagoKeys(){
		$scope.short_name 	= 'mp-app-3598751';
		$scope.public_key 	= 'TEST-34834c85-d364-430b-b160-6e0d153bc9b1';
		$scope.access_token = 'TEST-5999233168669110-070409-c45688c22a767978116eb22338694e3a__LD_LA__-3598751';
	}

	function recordOrder(){
		
		if(sessionStorage.getItem('PedidoId') == ''){
			// console.log('Gravando Pedido...');
			var pedidos = [];
			var cliente_id = localStorage.getItem('cliente.id');
			compras = JSON.parse(sessionStorage.getItem('compras'));
			angular.forEach(compras, function(value, index){
				pedidos.push({
					empresa_id: parseInt(value.Servico.empresa_id),
					servico_id: parseInt(value.Servico.id),
					quantidade: parseInt(value.Servico.quantidade),
					valor: parseFloat(value.Servico.valor),
					total: parseFloat(value.Servico.total),
					taxa_entrega: 0.00
				});
			});
			
			var order = {
				'cliente_id' : localStorage.getItem('cliente.id'),
				'cep_entrega' : $scope.dadospagto.entrega.Endereco.cep,
				'nro_entrega' : $scope.dadospagto.entrega.Endereco.numero,
				'cliente_endereco_entrega_id' : $scope.dadospagto.entrega.Endereco.id,
				'total_valor' : $scope.dadospagto.transactionAmount.replace(',', '.'),
				'total_taxa_entrega' : $scope.dadospagto.totalShipping.replace(',', '.'),
				'pagamento_tipo_id' : $scope.dadospagto.paymentTypeId,
				'servicos' : pedidos
			};
			
			webService.saveOrder(order).then(function(data){
				if(data.pedido_id){
					sessionStorage.setItem('PedidoId', data.pedido_id);
					// console.log('Pedido Salvo com Sucesso: ' + data.pedido_id);
				}
			});
		}
	}

	function initPagamento(){
		if(sessionStorage.getItem('valor_total')){
			console.log('Inicializando Mercado Pago');
			getMercadoPagoKeys();
			Mercadopago.setPublishableKey($scope.public_key);	
			Mercadopago.getIdentificationTypes();
			$scope.getPayments();
			recordOrder();
		}else{
			$scope.backHome();
		}
	}

	function validaForm(){
		var error = [];
		if($scope.dadospagto.email == '') error.push('Preencha o Email');
		if($scope.dadospagto.cardNumber == '') error.push('Preencha o Número do Cartão');
		if($scope.dadospagto.cardExpirationMonth == '') error.push('Preencha o Mês de Validade');
		if($scope.dadospagto.cardExpirationYear == '') error.push('Preencha o Ano de Validade');
		if($scope.dadospagto.cardholderName == '') error.push('Preencha o Nome no Cartão');
		if($scope.dadospagto.securityCode == '') error.push('Preencha o Código de Segurança');
		if($scope.dadospagto.docNumber == '') error.push('Preencha o Documento');
		if($scope.dadospagto.entrega == null) error.push('Selecione o Endereço de Entrega');
		if($scope.dadospagto.installments == null) error.push('Selecione o pagamento');
		if(error != ''){
			$scope.errors = error;
			return false;
		}else{
			return true;
		}
		
	}

	$scope.endPayment = function(){
		$scope.errors = [];
		if(validaForm()){
			$scope.show('Efetuando pagamento. Aguarde...');
			Mercadopago.getIdentificationTypes();
			formPay = document.querySelector('#pay');
			Mercadopago.createToken(formPay, function(status, response){
				if (status != 200 && status != 201) {
					$scope.errors.push('Erro ao adquirir toke do marcado pago');
					$scope.hide();
					$state.go('app.pagamento');
				}else{
					// console.log('Registrando Token Mercado Pago: ' + response.id);
					$scope.dadospagto.token = response.id;	
					var payment = {
						token : response.id,
						installments : $scope.dadospagto.installments.installments,
						payment_method_id : $scope.dadospagto.paymentMethodId,
						payer_email : $scope.dadospagto.email,
						transaction_amount : $scope.dadospagto.transactionAmount.replace(',', '.'),
						pedido_id : sessionStorage.getItem('PedidoId'),
						nro_entrega : $scope.dadospagto.entrega.Endereco.numero,
						cliente_endereco_entrega_id : $scope.dadospagto.entrega.Endereco.id,
						total_taxa_entrega : $scope.dadospagto.totalShipping.replace(',', '.'),
						total_valor :  $scope.dadospagto.transactionAmount.replace(',', '.')
					}
					webService.setPgtoMercadoPago(payment).then(function(data){
						if(data.success){
							sessionStorage.removeItem('PedidoId');
							sessionStorage.removeItem('cep_entrega');
							sessionStorage.removeItem('sub_total');
							sessionStorage.removeItem('taxa_de_entrega');
							sessionStorage.removeItem('valor_total');
							sessionStorage.removeItem('compras');
							$scope.goHome('app.finalizado', {pedidoId: data.pedido_id});
						}
						$scope.hide();
					});
				}	
			});
		}
	}

	$scope.show();
	getClienteEnderecos();
	setTimeout(function(){
		initPagamento();
		$scope.hide();
	}, 2000);


}]);



