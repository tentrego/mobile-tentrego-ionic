angular.module('starter.locatectrl', []).controller('LocateCtrl', ['$scope', '$ionicPopup', '$interval', '$timeout', function($scope, $ionicPopup, $interval, $timeout){

	$scope.refresh = function(){
		$scope.show();
		$scope.getGeoLocation();
		$timeout(function(){
			$scope.latitude  = localStorage.getItem('geo.latitude');
			$scope.longitude = localStorage.getItem('geo.longitude');
			$scope.refreshMap();		
			$scope.hide();
		}, 5000);
	}

	$scope.refreshMap = function(){
		try{
		    var latLng = new google.maps.LatLng($scope.latitude, $scope.longitude);
		    var mapOptions = {
		      center: latLng,
		      zoom: 15,
		      mapTypeId: google.maps.MapTypeId.ROADMAP
		    };
		    $scope.map = new google.maps.Map(document.getElementById("map"), mapOptions);	
			var marker = new google.maps.Marker({
			    position: latLng,
			    title:"Estou aqui!!!"
			});
			marker.setMap($scope.map);    
		}catch(e){
			console.log ("Error Message: " + e.message);
		}

		getPlaces($scope.latitude, $scope.longitude);

	}

	function getPlaces(latitude, longitude) {
	    var latLong = new google.maps.LatLng(latitude, longitude);
	    var mapOptions = {
	        center: latLong,
	        zoom: 15,
	        mapTypeId: google.maps.MapTypeId.ROADMAP
	    };
	    Map = new google.maps.Map(document.getElementById("map"), mapOptions);
	    Infowindow = new google.maps.InfoWindow();
		var marker = new google.maps.Marker({
		    position: latLong,
		    title:"Estou aqui!!!"
		});
		marker.setMap(Map);  	    
	    var service = new google.maps.places.PlacesService(Map);
	    service.nearbySearch({
	        location: latLong,
	        radius: 500,
	        type: ['store']
	    }, foundStoresCallback);
	}	

	function foundStoresCallback(results, status) {
	    if (status === google.maps.places.PlacesServiceStatus.OK) {
	        for (var i = 0; i < results.length; i++) {
	            createMarker(results[i]);
	        }
	    }
	}	

	function createMarker(place) {
	    var placeLoc = place.geometry.location;
	    var marker = new google.maps.Marker({
	        map: Map,
	        position: place.geometry.location
	    });
	    google.maps.event.addListener(marker, 'click', function () {
	        Infowindow.setContent(place.name);
	        Infowindow.open(Map, this);
	    });
	}

	$scope.refresh();

}]);