angular.module('starter.controllers', [
		'ngFileUpload', 'ngImgCrop',
		'starter.homectrl',
		'starter.empresasctrl', 'starter.ofertasctrl',  'starter.configctrl', 'starter.searchctrl', 'starter.favoritoctrl', 
		'starter.cadastroctrl', 'starter.dadosctrl', 'starter.enderecosctrl', 'starter.pedidosctrl', 'starter.carrinhoctrl', 
		'starter.pagamentoctrl'
]).controller('AppCtrl', ['$scope', '$q', '$http', '$state', '$ionicModal', '$ionicPopup', '$timeout', '$ionicLoading', '$ionicHistory', 
'$cordovaGeolocation', '$rootScope', 'webService', function($scope, $q, $http, $state, $ionicModal, $ionicPopup, $timeout, $ionicLoading, 
$ionicHistory, $cordovaGeolocation, $rootScope, webService) {

	// Form data for the login modal
	$scope.loginData = {};
	$scope.ofertas 	 = {};
	$scope.baseUrl	 = 'https://www.tentrego.com.br/tentrego/';
	$scope.avatar 	 = 'img/avatar/default-avatar.png';
	$scope.id 		 = 0;
	$scope.page 	 = window.location.hash;


	$scope.getAvatar = function(){ return localStorage.getItem('cliente.avatar'); }

	$scope.active = function(opc){
		document.getElementById('menu_footer_card').className = 'tab-item';
		document.getElementById('menu_footer_home').className = 'tab-item';
		document.getElementById('menu_footer_favorite').className = 'tab-item';
		document.getElementById('menu_footer_search').className = 'tab-item';
		document.getElementById('menu_footer_' + opc).className = 'tab-item active';
	}

	// Create the login modal that we will use later
	$ionicModal.fromTemplateUrl('templates/login.html', {
		scope: $scope,
		animation: 'slide-in-up'
	}).then(function(modal) {
		$scope.modal = modal;
	});

	$scope.openCadastro = function(){
		$scope.closeLogin();
		$state.go('app.cadastro');
	}

	$scope.getCliente = function(){
		$scope.clienteid = localStorage.getItem('cliente.id');
		$scope.avatar	 = localStorage.getItem('cliente.avatar');
		$scope.nome	 	 = localStorage.getItem('cliente.nome');
		$scope.sobrenome = localStorage.getItem('cliente.sobrenome');
		$scope.email 	 = localStorage.getItem('cliente.email');
		$scope.auth      = localStorage.getItem('cliente.auth');
		return JSON.parse(localStorage.getItem('cliente'));
	}

	$scope.setCliente = function(cliente){
		localStorage.setItem('cliente', JSON.stringify(cliente));
		localStorage.setItem('cliente.id', cliente['id']);
		localStorage.setItem('cliente.avatar', cliente['avatar']);
		localStorage.setItem('cliente.nome', cliente['nome']);
		localStorage.setItem('cliente.sobrenome', cliente['sobrenome']);
		localStorage.setItem('cliente.email', cliente['email']);
		localStorage.setItem('cliente.auth', cliente['auth']);
	}

	$scope.getGeoLocation = function(){
		
		var onSuccess = function(position) {
			localStorage.setItem('geo.latitude', position.coords.latitude);
			localStorage.setItem('geo.longitude', position.coords.longitude);
			//localStorage.setItem('geo.accuracy', position.coords.accuracy);
			//localStorage.setItem('geo.altitude', position.coords.altitude);
			//localStorage.setItem('geo.heading', position.coords.heading);
			//localStorage.setItem('geo.speed', position.coords.speed);
			//localStorage.setItem('geo.timestamp', position.timestamp);
			//localStorage.setItem('geo.updated', dt.toString());
		};

		// onError Callback receives a PositionError object
		function onError(error) {
		    // alert('code: '    + error.code    + '\n' + 'message: ' + error.message + '\n');
		}

		var geoSettings = { maximumAge: 5000, timeout: 7000, enableHighAccuracy: true };

		navigator.geolocation.getCurrentPosition(onSuccess, onError, geoSettings);
	}

	$scope.show = function(message = 'Carregando...'){
		console.log(message);
		$scope.loadingMessage = message;
		try{
			$ionicLoading.show({
				template: message,
				templateUrl: 'templates/loading.html',
				scope: $scope,
				content: 'Loading',
				animation: 'fade-in',
				showBackdrop: true,
				maxWidth: 200,
				showDelay: 0
			});
		}catch(e){
			console.log('Message Error: ' + e.message);
		}
	};

	$scope.hide = function(){
		$ionicLoading.hide();
	}

	// Triggered in the login modal to close it
	$scope.closeLogin = function() {
		$scope.modal.hide();
	};

	// Open the login modal
	$scope.login = function() {
		$scope.modal.show();
	};

	$scope.closeApp = function(){
		ionic.Platform.exitApp();
	}

	$scope.goHome = function(path, params = null){
		$ionicHistory.nextViewOptions({
	  		disableAnimate: false,
			disableBack: true,
			historyRoot: true
		});
		$state.go(path, params);
	}

	$scope.backHome = function(){
		$ionicHistory.nextViewOptions({
	  		disableAnimate: false,
			disableBack: true,
			historyRoot: true
		});
		$state.go('app.home');
	}

	$scope.logout = function(){
		localStorage.removeItem('cliente');
		localStorage.removeItem('cliente.id');
		localStorage.removeItem('cliente.avatar');
		localStorage.removeItem('cliente.nome');
		localStorage.removeItem('cliente.sobrenome');
		localStorage.removeItem('cliente.email');
		localStorage.removeItem('cliente.auth');
		localStorage.removeItem('geo.latitude');
		localStorage.removeItem('geo.longitude');
		$scope.logado = false;
		$scope.backHome();
	}

	$scope.auth = function(){
		$scope.clienteId = localStorage.getItem('cliente.id');
		$scope.token = localStorage.getItem('cliente.auth');
		$scope.show();
		webService.getAuthenticate($scope.token, $scope.clienteId).then(function(data){
			if(data != null){

			}
		})
	}

	$scope.doLogin = function() {
		$scope.show('Logando...');
		webService.loginMobile($scope.loginData.username, $scope.loginData.password).then(function(data){
			if(data != null){
				if(data.id){
					$scope.setCliente(data);
					$scope.logado = true;
					$timeout(function() { $scope.closeLogin(); $scope.hide(); }, 1000);
				}else{
					alert(data.message);
					$scope.hide();
				}
			}else{
				$scope.hide();
				console.log('Erro ao efetuar o logon');
			}
		});
	};

	$scope.alert = function(title, message, action = null){
		$ionicPopup.alert({
			title: title,
			template: message,
			buttons: [{ 
	    		text: 'OK',
	    		type: 'button-dark',
	    		onTap: function(e) {
	    			switch(action){
	    				case 'home': $scope.backHome(); break;
	    				case 'back': $ionicHistory.goBack(-1); break;
	    			}
	    		}
	  		}]	
		});	
	}

	$scope.verFavorito = function(id){
		var favoritos = {};
		var find = false;
		if(typeof localStorage.favoritos != 'undefined') favoritos = JSON.parse(localStorage.getItem('favoritos'));	
		for(x = 0; x < favoritos.length; x++) if(favoritos[x].Servico.id == id) find = true;
		return find;	
	}
	
	$scope.removerAcentos = function(newStringComAcento){
	  var string = newStringComAcento;
		var mapaAcentosHex 	= {
			a : /[\xE0-\xE6]/g,
			e : /[\xE8-\xEB]/g,
			i : /[\xEC-\xEF]/g,
			o : /[\xF2-\xF6]/g,
			u : /[\xF9-\xFC]/g,
			c : /\xE7/g,
			n : /\xF1/g
		};
		for ( var letra in mapaAcentosHex ) {
			var expressaoRegular = mapaAcentosHex[letra];
			string = string.replace( expressaoRegular, letra );
		}
		return string;
	}	

	$scope.getCoordenadas = function(endereco){
		var defferer = $q.defer();
		var street = $scope.removerAcentos(endereco.street.trim().split(' ').join('+'));
		var bairro = $scope.removerAcentos(endereco.bairro.trim().split(' ').join('+'));
		var cidade = $scope.removerAcentos(endereco.cidade.trim().split(' ').join('+'));
		var estado = $scope.removerAcentos(endereco.estado.trim().split(' ').join('+'));
		var number = endereco.numero;
		street = street.replace('Rua', 'R.');
		street = street.replace('Avenida', 'Av.');
		var address = street + ',+' + number + '+-+' + bairro + ',+' + cidade + '+-+' + estado + ',+Brasil';
		if(street != '' && number != '' && bairro != '' && cidade != '' && estado != ''){
			$http({
  				method: 'GET',
  				url: 'https://maps.googleapis.com/maps/api/geocode/json?address=' + address + '&key=AIzaSyDveK1N467Zbi-eQ5X_a_Ah-LY9pnCO9fA'
			}).then(function successCallback(retorno) {
				defferer.resolve({
					success: true,
					latitude: retorno.data.results[0].geometry.location.lat,
					longitude: retorno.data.results[0].geometry.location.lng
				});
  			}, function errorCallback(retorno) {
  				defferer.resolve({success:false});
		  	});
		}else{
			defferer.resolve({success:false});
		}
		return defferer.promise;
	}


	// grab pointer to original function
	var oldSoftBack = $rootScope.$ionicGoBack;
	$rootScope.$ionicGoBack = function() {
		document.getElementById('menu_footer_card').className = 'tab-item';
		document.getElementById('menu_footer_home').className = 'tab-item';
		document.getElementById('menu_footer_favorite').className = 'tab-item';
		document.getElementById('menu_footer_search').className = 'tab-item';
	    oldSoftBack();
	};

	if(localStorage.getItem('cliente.auth')){
		$scope.logado = true;
		$scope.getCliente();
	}else{
		$scope.logado 	 = false;
	}

}]);
