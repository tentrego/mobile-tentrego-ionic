angular.module('angularSoap', [])

.factory("$soap",['$q',function($q){
	return {
		post: function(url, action, params){
			var deferred = $q.defer();
			
			//Create SOAPClientParameters
			var soapParams = new SOAPClientParameters();
			for(var param in params){
				soapParams.add(param, params[param]);
			}
			
			//Create Callback
			var soapCallback = function(e){
				if(e){
					if(e.constructor.toString().indexOf("function Error()") != -1){
						deferred.reject("An error has occurred.");
					} else {
						try{
							deferred.resolve(e);
						}catch(ex){
							console.log(ex);
						}
						
					}
				}else{
					deferred.resolve({response:{}});
				}
			}
			SOAPClient.invoke(url, action, soapParams, true, soapCallback);
			return deferred.promise;
		},
		setCredentials: function(username, password){
			SOAPClient.username = username;
			SOAPClient.password = password;
		}
	}
}]);
