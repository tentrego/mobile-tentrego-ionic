angular.module('starter.services', ['angularSoap']).service('webService', ['$soap', '$q', function($soap, $q){

	var baseUrl = "http://www.tentrego.com.br/tentrego/webservice/mobile.php";

	$soap.setCredentials("username", "password");

	this.getEmpresas = function(categoria = null){
		var defferer = $q.defer();
		try{
			latitude = localStorage.getItem('geo.latitude');
			longitude = localStorage.getItem('geo.longitude');
			$soap.post(baseUrl, "getEmpresas", {categoria: categoria, latitude: latitude, longitude: longitude}).then(function(data) {
				if(data.response != ''){
					data = JSON.parse(data.response);
		        	defferer.resolve(data);
				}
	    	});
		}catch(e){
			console.log ("Error Message: " + e.message);
		    console.log ("Error Code: " + e.number);
		    console.log ("Error Name: " + e.name);	
		    defferer.resolve({});
		}
    	return defferer.promise;
	}
	
	this.getEmpresaById = function(id){
		var defferer = $q.defer();
		try{
			latitude = localStorage.getItem('geo.latitude');
			longitude = localStorage.getItem('geo.longitude');
		    $soap.post(baseUrl, "getEmpresaById", {id: id, latitude: latitude, longitude: longitude}).then(function(data) {
		    	if(Object.keys(data.response).length > 0){
			        data = JSON.parse(data.response);
			        defferer.resolve(data);
		    	}
		    });
		}catch(e){
			console.log ("Error Message: " + e.message);
		    console.log ("Error Code: " + e.number);
		    console.log ("Error Name: " + e.name);	
		    defferer.resolve({});
		}
	    return defferer.promise;
	}

	this.getOfertas = function(categoria = ''){
		var defferer = $q.defer();
		funcOferta = (categoria) ? "getOfertasByCategoria" : "getOfertas";
		$soap.post(baseUrl, funcOferta, {categoria: categoria}).then(function(data) {
			try{
				data = JSON.parse(data.response);
	        	defferer.resolve(data);
			}catch(e){
				console.log ("Error Message: " + e.message);
			    console.log ("Error Code: " + e.number);
			    console.log ("Error Name: " + e.name);	
			    defferer.resolve({});
			}
    	});
    	return defferer.promise;
	}

	this.getOfertaById = function(id){
		var defferer = $q.defer();
	    $soap.post(baseUrl, "getOfertaById", {id: id}).then(function(data) {
	    	try{
		        data = JSON.parse(data.response);
		        defferer.resolve(data);
	    	}catch(e){
				console.log ("Error Message: " + e.message);
			    console.log ("Error Code: " + e.number);
			    console.log ("Error Name: " + e.name);	
			    defferer.resolve({});
	    	}
	    });
	    return defferer.promise;
	}

	this.getOfertasByEmpresa = function(id){
		var defferer = $q.defer();
	    $soap.post(baseUrl, "getOfertasByEmpresa", {id: id}).then(function(data) {
	    	try{
		        data = JSON.parse(data.response);
		        defferer.resolve(data);
	    	}catch(e){
				console.log ("Error Message: " + e.message);
			    console.log ("Error Code: " + e.number);
			    console.log ("Error Name: " + e.name);	
			    defferer.resolve({});
	    	}
	    });
	    return defferer.promise;
	}

	this.getOfertasByKeyword = function(keyword){
		var defferer = $q.defer();
	    $soap.post(baseUrl, "getOfertasByKeyword", {keyword: keyword}).then(function(data) {
	    	try{
		        data = JSON.parse(data.response);
		        defferer.resolve(data);
	    	}catch(e){
				console.log ("Error Message: " + e.message);
			    console.log ("Error Code: " + e.number);
			    console.log ("Error Name: " + e.name);	
			    defferer.resolve({});
	    	}
	    });
	    return defferer.promise;
	}

	this.getCategorias = function(){
		var defferer = $q.defer();
	    $soap.post(baseUrl, "getCategorias").then(function(data) {
	    	try{
		        data = JSON.parse(data.response);
		        defferer.resolve(data);
	    	}catch(e){
				console.log ("Error Message: " + e.message);
			    console.log ("Error Code: " + e.number);
			    console.log ("Error Name: " + e.name);	
			    defferer.resolve({});
	    	}
	    });
	    return defferer.promise;
	}

	this.loginMobile = function(username, password){
		var defferer = $q.defer();
	    $soap.post(baseUrl, "loginMobile", {user: username, pass: password}).then(function(data) {
	    	try{
		        data = JSON.parse(data.response);
		        defferer.resolve(data);
	    	}catch(e){
				console.log ("Error Message: " + e.message);
			    console.log ("Error Code: " + e.number);
			    console.log ("Error Name: " + e.name);	
			    defferer.resolve({});
	    	}
	    });
	    return defferer.promise;
	}

	this.getAuthenticate = function(cliente_id, token){
		var defferer = $q.defer();
	    $soap.post(baseUrl, "getAuthenticate", {cliente_id: cliente_id, token: token}).then(function(data) {
	    	try{
		        data = JSON.parse(data.response);
		        defferer.resolve(data);
	    	}catch(e){
				console.log ("Error Message: " + e.message);
			    console.log ("Error Code: " + e.number);
			    console.log ("Error Name: " + e.name);	
			    defferer.resolve({});
	    	}
	    });
	    return defferer.promise;
	}

	this.saveCliente = function(cliente){
		var defferer = $q.defer();
		json = JSON.stringify(cliente);
	    $soap.post(baseUrl, "saveCliente",  {dados: json} ).then(function(data) {
	    	try{
		        data = JSON.parse(data.response);
		        defferer.resolve(data);
	    	}catch(e){
				console.log ("Error Message: " + e.message);
			    console.log ("Error Code: " + e.number);
			    console.log ("Error Name: " + e.name);	
			    defferer.resolve({});
	    	}
	    });
	    return defferer.promise;
	}

	this.getPedidos = function(id){
		var defferer = $q.defer();
	    $soap.post(baseUrl, "getPedidos",  {id: id} ).then(function(data) {
	    	try{
		        data = JSON.parse(data.response);
		        defferer.resolve(data);
	    	}catch(e){
				console.log ("Error Message: " + e.message);
			    console.log ("Error Code: " + e.number);
			    console.log ("Error Name: " + e.name);	
			    defferer.resolve({});
	    	}
	    });
	    return defferer.promise;
	}

	this.getPedidoById = function(id){
		var defferer = $q.defer();
		$soap.post(baseUrl, "getPedidoById",  {id: id} ).then(function(data) {
			try{
				data = JSON.parse(data.response);
				defferer.resolve(data);
			}catch(e){
				console.log ("Error Message: " + e.message);
			    console.log ("Error Code: " + e.number);
			    console.log ("Error Name: " + e.name);	
			    defferer.resolve({});
			}
		});
		return defferer.promise;
	}

	this.changePassw = function(oPass, nPass, cPass){
		var defferer = $q.defer();
		var id = localStorage.getItem('cliente.id');
		$soap.post(baseUrl, "changePassw",  {id:id, opass:oPass, npass:nPass, cpass:cPass} ).then(function(data) {
			try{
				data = JSON.parse(data.response);
				defferer.resolve(data);
			}catch(e){
				console.log ("Error Message: " + e.message);
				console.log ("Error Code: " + e.number);
				console.log ("Error Name: " + e.name);	
				defferer.resolve({});
			}
		});
		return defferer.promise;
	}

	this.getTransportValue = function(nroCep){
		var defferer = $q.defer();
		var compras = JSON.parse(sessionStorage.getItem('compras'));
		var ids = new Array();
		for(x = 0; x < compras.length; x++) ids.push(compras[x].Servico.empresa_id);
		$soap.post(baseUrl, "getTransportValue",  {ids:JSON.stringify(ids), cep:nroCep}).then(function(data) {
			try{
				data = JSON.parse(data.response);
				defferer.resolve(data);
			}catch(e){
				console.log ("Error Message: " + e.message);
				console.log ("Error Code: " + e.number);
				console.log ("Error Name: " + e.name);	
				defferer.resolve({});
			}
		});
		return defferer.promise;
	}

	this.saveOrder = function(dados){
		var defferer = $q.defer();
		$soap.post(baseUrl, "saveOrder", {pedido:JSON.stringify(dados)}).then(function(data) {
			try{
				data = JSON.parse(data.response);
				defferer.resolve(data);
			}catch(e){
				console.log ("Error Message: " + e.message);
				console.log ("Error Code: " + e.number);
				console.log ("Error Name: " + e.name);	
				defferer.resolve({});
			}
		});
		return defferer.promise;
	}

	this.setPgtoMercadoPago = function(dados){
		var defferer = $q.defer();
		$soap.post(baseUrl, "setPgtoMercadoPago",  { dadospgto : unescape(encodeURIComponent(JSON.stringify(dados))) }).then(function(data) {
			try{
				data = JSON.parse(data.response);
				defferer.resolve(data);
			}catch(e){
				console.log ("Error Message: " + e.message);
				console.log ("Error Code: " + e.number);
				console.log ("Error Name: " + e.name);	
				defferer.resolve({});
			}
		});
		return defferer.promise;
	}

	this.callWService = function(func, params = {}){
		var defferer = $q.defer();
		$soap.post(baseUrl, func, params).then(function(data) {
			try{
				data = JSON.parse(data.response);
				defferer.resolve(data);
			}catch(e){
				console.log ("Error Message: " + e.message);
				console.log ("Error Code: " + e.number);
				console.log ("Error Name: " + e.name);	
				defferer.resolve({});
			}
		});
		return defferer.promise;
	}

}]);