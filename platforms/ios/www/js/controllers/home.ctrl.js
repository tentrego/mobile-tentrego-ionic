angular.module('starter.homectrl', ['ngCordova'])
.controller('HomeCtrl', ['$scope', '$interval', '$cordovaGeolocation', '$ionicHistory', function($scope, $interval, $cordovaGeolocation, $ionicHistory){

	function getGeoLocation(){
		var geoSettings = {frequency: 10000, timeout: 100000, enableHighAccuracy: false};
		var geo = $cordovaGeolocation.getCurrentPosition(geoSettings);
		geo.then(function (position) {
			dt = new Date();
			localStorage.setItem('latitude', position.coords.latitude);
			localStorage.setItem('longitude', position.coords.longitude);
			localStorage.setItem('updated', dt.toString());
		},
		function error(err) {
			$scope.errors = err;
		});
	}

	$interval(function(){ getGeoLocation(); }, 10000);
	
	getGeoLocation();

	$scope.first = function() {
		$ionicHistory.goBack();
	};
	
}])