angular.module('starter.controllers', ['ngCordova', 'starter.homectrl', 'starter.ofertasctrl', 'starter.configctrl', 'starter.cameractrl'])

.controller('AppCtrl', ['$scope', '$ionicModal', '$timeout', '$ionicLoading', function($scope, $ionicModal, $timeout, $ionicLoading) {
// Form data for the login modal
$scope.loginData = {};
$scope.ofertas = {};

// Create the login modal that we will use later
$ionicModal.fromTemplateUrl('templates/login.html', {
	scope: $scope
}).then(function(modal) {
	$scope.modal = modal;
});

$scope.go = function () {

};

$scope.show = function(){
	$ionicLoading.show({
		content: 'Loading',
		animation: 'fade-in',
		showBackdrop: true,
		maxWidth: 200,
		showDelay: 0
	}).then(function(){ });
};

$scope.hide = function(){
	$ionicLoading.hide().then(function(){ });
}

// Triggered in the login modal to close it
$scope.closeLogin = function() {
	$scope.modal.hide();
};

// Open the login modal
$scope.login = function() {
	$scope.modal.show();
};

$scope.closeApp = function(){
	console.log('Fechando Aplicatico');
	ionic.Platform.exitApp();
}

// Perform the login action when the user submits the login form
$scope.doLogin = function() {
	console.log('Doing login', $scope.loginData);

	// Simulate a login delay. Remove this and replace with your login
	// code if using a login system
	$timeout(function() {
		$scope.closeLogin();
	}, 1000);
};

}]);