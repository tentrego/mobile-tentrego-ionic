angular.module('starter.cameractrl', ['ngCordova'])
.controller('CameraCtrl', ['$scope', '$cordovaCamera', function($scope, $cordovaCamera){

	$scope.takePicture = function(){
		var options = {
			destinationType:Camera.DestinationType.DATA_URL,
			encodingType:Camera.EncodingType.JPEG,
			saveToPhotoAlbum:true,
			cameraDirection:0
		}
		$cordovaCamera.getPicture(options)
		.then(function(data){
			console.log('Camera data: ' + angular.toJson(data));
			$scope.pictureUrl = 'data:image/jpeg;base64, ' + data;
		}, function(err){
			console.log('Camera error: ' + angular.toJson(data));
		});
	}

	$scope.getPicture = function () {
		$cordovaCamera.getPicture({})
		.then(function(data) {
			console.log('Camera data: ' + angular.toJson(data));
			$scope.picture = data;
		}, function(err) {
			console.log('Camera error: ' + angular.toJson(data));
			console.log(err);
		});
	};     

}]);
