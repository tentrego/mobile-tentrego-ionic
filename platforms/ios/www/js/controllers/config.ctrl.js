angular.module('starter.configctrl', ['ngCordova'])
.controller('ConfigCtrl', ['$scope', '$cordovaGeolocation', function($scope, $cordovaGeolocation){

	$cordovaGeolocation.getCurrentPosition().then(function(position){
		console.log(position);
	});

	/*
	function getGeoLocalizacao(){

		if(navigator.geolocation){
			// $scope.show();
	 		navigator.geolocation.getCurrentPosition(function(position) {
				$scope.latitude  = position.coords.latitude;
				$scope.longitude = position.coords.longitude;
				// $scope.hide();
			});
		}else{
			window.plugins.toast.showShortCenter("Geolocation is not supported by this browser.");
		}

		
		var posOptions = {timeout: 3000, enableHighAccuracy: false};
		$cordovaGeolocation.getCurrentPosition(posOptions).then(function (position) {
			$scope.latitude  = position.coords.latitude;
			$scope.longitude = position.coords.longitude;
			alert('getCurrentPosition: ' + $scope.latitude + ' - ' + $scope.longitude);
		}, function(err) {
			console.log(err);
			// error
		});

		var watchOptions = {timeout : 3000,	enableHighAccuracy: false };
		var watch = $cordovaGeolocation.watchPosition(watchOptions);
		watch.then(null, function(err) {
			console.log(err); // error
		},
		function(position) {
			var lat  = position.coords.latitude;
			var long = position.coords.longitude;
			$scope.latitude = lat;
			$scope.longitude = long;
			alert('watchPosition: ' + $scope.latitude + ' - ' + $scope.longitude);
		});
		watch.clearWatch();

		$cordovaGeolocation.clearWatch(watch);
		
	}
	*/

	$scope.latitude = localStorage['latitude'];
	$scope.longitude = localStorage['longitude'];

	$scope.deviceInformation = ionic.Platform.device();
	$scope.isWebView = ionic.Platform.isWebView();
	$scope.isIPad = ionic.Platform.isIPad();
	$scope.isIOS = ionic.Platform.isIOS();
	$scope.isAndroid = ionic.Platform.isAndroid();
	$scope.isWindowsPhone = ionic.Platform.isWindowsPhone();
	$scope.currentPlatform = ionic.Platform.platform();
	$scope.currentPlatformVersion = ionic.Platform.version();

	// getGeoLocalizacao();

	/*
	console.log($scope.deviceInformation);
	console.log('Android: ' + $scope.isAndroid);
	console.log('Web: ' + $scope.isWebView);
	console.log('IPad: ' + $scope.isIPad);
	console.log('IOS: ' + $scope.isIOS);
	console.log('WindowsPhone: ' + $scope.isWindowsPhone);
	console.log('Plataforma: ' + $scope.currentPlatform);
	console.log('Plataforma Versão: ' + $scope.currentPlatformVersion);
	*/

}]);
