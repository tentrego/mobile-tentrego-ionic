angular.module('starter.ofertasctrl', [])
.controller('OfertasCtrl', ['$scope', '$stateParams', 'webService', function($scope, $stateParams, webService){
	
	function getOfertas(categoria){
		$scope.show();
		$scope.noresults = false;
		webService.getOfertas(categoria).then(function(data){ 
			if(data != null){
				first = data[0];
				$scope.title = (categoria) ? first.Servico.categoria : 'Ofertas';
				console.log($scope.title);
				$scope.categoria = first;
				$scope.ofertas = data; 		
			}else{
				$scope.ofertas = null;
				$scope.noresults = true;
				console.log('Nenhuma oferta encontrada');
			}
			$scope.hide();
		});
	}

	function getOfertaById(id){
		$scope.show();
		webService.getOfertaById(id).then(function(data){ 
			$scope.oferta = data[0];
			// localStorage.setItem('oferta', data[0]);
			$scope.hide();
		});		
	}

	if($stateParams.ofertaId){
		getOfertaById($stateParams.ofertaId);	
	}else{
		if($stateParams.categoria){
			getOfertas($stateParams.categoria);	
		}else{
			getOfertas();	
		}
	}
	
}]);