// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js

angular.module('starter', ['ionic', 'starter.controllers', 'starter.services', 'starter.factory'])
.run(function($ionicPlatform, $cordovaPush) {
	$ionicPlatform.ready(function() {
	// Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
	// for form inputs)
		if (window.cordova && window.cordova.plugins.Keyboard) {
			cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
			cordova.plugins.Keyboard.disableScroll(true);

		}
		if (window.StatusBar) {
			// org.apache.cordova.statusbar required
			StatusBar.styleDefault();
		}
	});

})

.config(function($stateProvider, $urlRouterProvider) {
	$stateProvider
	.state('app', {
		url: '/app',
		abstract: true,
		templateUrl: 'templates/menu.html',
		controller: 'AppCtrl'
	})
	.state('app.home', {
		url: '/home',
		views:{
			'menuContent':{
				templateUrl: 'templates/home.html',
				controller: 'HomeCtrl'
			}
		}
	})
	.state('app.ofertas', {
		url: '/ofertas/:categoria',
		views: {
			'menuContent':{
				templateUrl: 'templates/ofertas.html',
				controller: 'OfertasCtrl'
			}
		}
	})

	.state('app.dados', {
		url: '/dados',
		views: {
			'menuContent':{
				templateUrl: 'templates/dados.html',
				controller: 'OfertasCtrl'
			}
		}
	})

	.state('app.endereco', {
		url: '/endereco',
		views: {
			'menuContent':{
				templateUrl: 'templates/endereco.html',
				controller: 'OfertasCtrl'
			}
		}
	})

	.state('app.pedidos', {
		url: '/pedidos',
		views: {
			'menuContent':{
				templateUrl: 'templates/pedidos.html',
				controller: 'OfertasCtrl'
			}
		}
	})

	.state('app.pedido', {
		url: '/pedido',
		views: {
			'menuContent':{
				templateUrl: 'templates/pedido.html',
				controller: 'OfertasCtrl'
			}
		}
	})

	.state('app.oferta', {
		url: '/oferta/:ofertaId',
		views: {
			'menuContent':{
				templateUrl: 'templates/oferta.html',
				controller: 'OfertasCtrl'
			}
		}
	})
	.state('app.config', {
		url: '/config',
		views: {
			'menuContent' : {
				templateUrl: 'templates/config.html',
				controller: 'ConfigCtrl'
			}
		}
	})
	.state('app.search', {
		url: '/search',
		views: {
			'menuContent': {
				templateUrl: 'templates/search.html'
			}
		}
	})
	.state('app.camera', {
		url:'/camera',
		views:{
			'menuContent':{
				templateUrl:'templates/camera.html',
				controller: 'CameraCtrl'
			}
		}
	});
	// if none of the above states are matched, use this as the fallback
	$urlRouterProvider.otherwise('/app/home');
});
