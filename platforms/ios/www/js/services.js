angular.module('starter.services', ['angularSoap'])
.service('webService', ['$soap', '$q', function($soap, $q){

	var baseUrl = "http://www.tentrego.com.br/tentrego/webservice/mobile.php";

	$soap.setCredentials("username","password");
	
	this.getOfertas = function(categoria = ''){
		var defferer = $q.defer();
		funcOferta = (categoria) ? "getOfertasByCategoria" : "getOfertas";
		$soap.post(baseUrl, funcOferta, {categoria: categoria}).then(function(data) {
			try{
				data = JSON.parse(data.response);
	        	defferer.resolve(data);
			}catch(ex){
				defferer.resolve({});
				console.log(ex);				
			}
    	});
    	return defferer.promise;
	}

	this.getOfertasHome = function(){
		var defferer = $q.defer();
		$soap.post(baseUrl, "getOfertasHome").then(function(data) {
			data = JSON.parse(data.response);
        	defferer.resolve(data);
    	});
    	return defferer.promise;
	}

	this.getOfertaById = function(id){
		var defferer = $q.defer();
	    $soap.post(baseUrl, "getOfertaById", {id: id}).then(function(data) {
	    	if(data.response){
		        data = JSON.parse(data.response);
		        defferer.resolve(data);
	    	}else{
	    		console.log('asdasdasd');
	    	}
	    });
	    return defferer.promise;
	}

	this.getCategorias = function(){
		var defferer = $q.defer();
	    $soap.post(baseUrl, "getCategorias").then(function(data) {
	    	if(data.response){
		        data = JSON.parse(data.response);
		        defferer.resolve(data);
	    	}else{
	    		console.log('asdasdasd');
	    	}
	    });
	    return defferer.promise;
	}

}]);